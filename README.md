# Pscws

本类库基于是 `PSCWS4` 简单开发而来，主要是将其封装为正常额 composer 包，方便调用，其次稍微修改了xdb 文件操作的 调用流程

## 关于 PSCWS4

PSCWS4 是由 hightman 于 2007 年开发的纯 PHP 代码实现的简易中文分词系统第四版的简称。

PSCWS 是英文 PHP Simple Chinese Words Segmentation 的头字母缩写，它是 SCWS 项目的前身。
现 SCWS 已作为 FTPHP 项目的一个子项目继续发展，现于 2008-12 重新修订并整理发布。

SCWS 是一套开源并且免费的中文分词系统，提供优秀易用的 PHP 接口。
项目主页：http://www.ftphp.com/scws

PSCWS4 在算法、功能以及词典/规则文件格式上，完全兼容于 C 编写 libscws，类库的方法完全兼容
于 scws 的 php 扩展中的预定义类的方法。

第四版的算法采用N-核心词路径最优方案，采用强大的规则引擎识别人名地名数字等专有名词，C实现
的版本速度和效率均非常高，推荐使用。PSCWS4 是 SCWS(C) 的 PHP 实现，速度较慢。

## 类方法完全手册
(注: 构造函数可传入字符集作为参数, 这与另外调用 set_charset 效果是一样的)

    class Pscws {

        void set_preg_mode(int $model)
        说明：设置匹配模式 获取结果时将按照设定的 模式进行取值
        返回：无。
        参数：model 运行模式 缺省值：Pscws::PREG_MODE_ALL
            - Pscws::PREG_MODE_ALL(所有) 
            - Pscws::PREG_MODE_ALL(仅字典)

        void set_charset(string charset);
        说明：设定分词词典、规则集、欲分文本字符串的字符集，系统缺省是 gbk 字集。
        返回：无。
        参数：charset 是设定的字符集，目前只支持 utf8 和 gbk。（注：big5 也可作 gbk 处理）
        注意：输入要切分的文本，词典，规则文件这三者的字符集必须统一为该 charset 值。
        
        bool set_dict(string dict_fpath);
        说明：设置分词引擎所采用的词典文件。
        参数：dict_path 是词典的路径，可以是相对路径或完全路径。
        返回：成功返回 true 失败返回 false。
        错误：若有错误会给出 WARNING 级的错误提示。
        
        void set_rule(string rule_path);
        说明：设定分词所用的新词识别规则集（用于人名、地名、数字时间年代等识别）。
        返回：无。
        参数：rule_path 是规则集的路径，可以是相对路径或完全路径。
        
        void set_ignore(bool yes)
        说明：设定分词返回结果时是否去除一些特殊的标点符号之类。
        返回：无。
        参数：yes 设定值，如果为 true 则结果中不返回标点符号，如果为 false 则会返回，缺省为 false。
        
        void set_multi(int mode);
        说明：设定分词返回结果时是否复合分割，如“中国人”返回“中国＋人＋中国人”三个词。
        返回：无。
        参数：mode 设定值，1 ~ 15。
        按位异或的 1 | 2 | 4 | 8 分别表示: 短词 | 二元 | 主要单字 | 所有单字
        
        void set_duality(bool yes);
        说明：设定是否将闲散文字自动以二字分词法聚合。
        返回：无。
        参数：yes 设定值，如果为 true 则结果中多个单字会自动按二分法聚分，如果为 false 则不处理，缺省为 false。
        
        void set_debug(bool yes);
        说明：设置分词过程是否输出N-Path分词过程的调试信息。
        参数：yes 设定值，如果为 true 则分词过程中对于多路径分法分给出提示信息。
        返回：无。
        
        void send_text(string text)
        说明：发送设定分词所要切割的文本。
        返回：无。
        参数：text 是文本的内容。
        注意：执行本函数时，请先加载词典和规则集文件并设好相关选项。
        
        mixed get_result(void)
        说明：根据 send_text 设定的文本内容，返回一系列切好的词汇。
        返回：成功返回切好的词汇组成的数组， 若无更多词汇，返回 false。
        参数：无。
        注意：每次切割后本函数应该循环调用，直到返回 false 为止，因为程序每次返回的词数是不确定的。
        返回的词汇包含的键值有：word (string, 词本身) idf (folat, 逆文本词频) off (int, 在文本中的位置) attr(string, 词性)
        
        mixed get_tops( [int limit [, string attr]] )
        说明：根据 send_text 设定的文本内容，返回系统计算出来的最关键词汇列表。
        返回：成功返回切好的词汇组成的数组， 若无更多词汇，返回 false。
        参数：limit 可选参数，返回的词的最大数量，缺省是 10；
        attr 可选参数，是一系列词性组成的字符串，各词性之间以半角的逗号隔开，
        这表示返回的词性必须在列表中，如果以~开头，则表示取反，词性必须不在列表中，
        缺省为空，返回全部词性，不过滤。
        
        string version(void);
        说明：返回本版号。
        返回：版本号（字符串）。
        参数：无。
        
        void close(void);
        说明：关闭释放资源，使用结束后可以手工调用该函数或等系统自动回收。
        返回：无。
        参数：无。
    }


## 词典导入与导出

```php
use Gaolei\Pscws\tools\XdbTool;

$xdbTool = XdbTool::init();

// txt 文件编译 xdb
$xdbTool->make('./dict/dict.txt','./dict/dict.xdb');

// xdb 反编译到 txt
$xdbTool->dump('./dict/dict.xdb','./dict/dict.txt');
```

### 关于txt 文件格式说明

关于文本文件的说明，每行一条记录，#开头表示注释，每条记录由
`word`(词)、`TF`(词频系数)、`IDF`(逆词频率系数)、`Attr`(北大标注法的词性)组成，这四个字段之间用 `\t`隔开。

### 北大词性标注版本

    Ag
    形语素
    形容词性语素。形容词代码为a，语素代码ｇ前面置以A。
    
    a
    形容词
    取英语形容词adjective的第1个字母。
    
    ad
    副形词
    直接作状语的形容词。形容词代码a和副词代码d并在一起。
    
    an
    名形词
    具有名词功能的形容词。形容词代码a和名词代码n并在一起。
    
    b
    区别词
    取汉字“别”的声母。
    
    c
    连词
    取英语连词conjunction的第1个字母。
    
    Dg
    副语素
    副词性语素。副词代码为d，语素代码ｇ前面置以D。
    
    d
    副词
    取adverb的第2个字母，因其第1个字母已用于形容词。
    
    e
    叹词
    取英语叹词exclamation的第1个字母。
    
    f
    方位词
    取汉字“方”
    
    g
    语素
    绝大多数语素都能作为合成词的“词根”，取汉字“根”的声母。
    
    h
    前接成分
    取英语head的第1个字母。
    
    i
    成语
    取英语成语idiom的第1个字母。
    
    j
    简称略语
    取汉字“简”的声母。
    
    k
    后接成分
    
    l
    习用语
    习用语尚未成为成语，有点“临时性”，取“临”的声母。
    
    m
    数词
    取英语numeral的第3个字母，n，u已有他用。
    
    Ng
    名语素
    名词性语素。名词代码为n，语素代码ｇ前面置以N。
    
    n
    名词
    取英语名词noun的第1个字母。
    
    nr
    人名
    名词代码n和“人(ren)”的声母并在一起。
    
    ns
    地名
    名词代码n和处所词代码s并在一起。
    
    nt
    机构团体
    “团”的声母为t，名词代码n和t并在一起。
    
    nz
    其他专名
    “专”的声母的第1个字母为z，名词代码n和z并在一起。
    
    o
    拟声词
    取英语拟声词onomatopoeia的第1个字母。
    
    ba 介词 把、将
    bei 介词 被
    p
    介词
    取英语介词prepositional的第1个字母。
    
    q
    量词
    取英语quantity的第1个字母。
    
    r
    代词
    取英语代词pronoun的第2个字母,因p已用于介词。
    
    s
    处所词
    取英语space的第1个字母。
    
    Tg
    时语素
    时间词性语素。时间词代码为t,在语素的代码g前面置以T。
    
    t
    时间词
    取英语time的第1个字母。
    
    dec 助词 的、之
    deg 助词 得
    di 助词 地
    etc 助词 等、等等
    as 助词 了、着、过
    msp 助词 所
    u
    其他助词
    取英语助词auxiliary
    
    Vg
    动语素
    动词性语素。动词代码为v。在语素的代码g前面置以V。
    
    v
    动词
    取英语动词verb的第一个字母。
    
    vd
    副动词
    直接作状语的动词。动词和副词的代码并在一起。
    
    vn
    名动词
    指具有名词功能的动词。动词和名词的代码并在一起。
    
    w
    其他标点符号
    
    x
    非语素字
    非语素字只是一个符号，字母x通常用于代表未知数、符号。
    
    y
    语气词
    取汉字“语”的声母。
    
    z
    状态词
    取汉字“状”的声母的前一个字母。